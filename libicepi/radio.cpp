/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of the software-based radio
 *
 *        Created:  26/12/2018 17:56:37
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "radio.h"
#include "hw/virtual_radio.h"

#include <cmath>
#include <memory>
#include <QDebug>

namespace IcePi {

Radio::Radio() :
    Radio(0,0)
{}

Radio::Radio(double minFrequency, double maxFrequency) :
    frequency(minFrequency),
    minFrequency(minFrequency),
    maxFrequency(maxFrequency)
{
    rdsTimer = new QTimer(this);
    connect(rdsTimer, &QTimer::timeout, this, &Radio::checkRDS);
    connect(this, &Radio::frequencyChanged, hw.get(), &IRadioHW::setFrequency);
    rdsTimer->start(500);
}

Radio::Radio(const Radio& radio) :
    frequency(radio.frequency),
    minFrequency(radio.minFrequency),
    maxFrequency(radio.maxFrequency)
{
    frequency = radio.frequency;
    rdsTimer = new QTimer(this);
    connect(rdsTimer, &QTimer::timeout, this, &Radio::checkRDS);
    connect(this, &Radio::frequencyChanged, hw.get(), &IRadioHW::setFrequency);
    rdsTimer->start(radio.rdsTimer->interval());
    hw.reset(radio.hw->clone());
}

Radio::Radio(Radio&& radio) :
    Radio(radio.minFrequency, radio.maxFrequency)
{
    frequency = radio.frequency;
    rdsTimer = radio.rdsTimer;
    radio.rdsTimer = nullptr;
    connect(rdsTimer, &QTimer::timeout, this, &Radio::checkRDS);
    connect(this, &Radio::frequencyChanged, hw.get(), &IRadioHW::setFrequency);
    hw = std::move(radio.hw);
}

Radio& Radio::operator=(const Radio& other) {
    minFrequency = other.minFrequency;
    maxFrequency = other.maxFrequency;
    setFrequency(other.frequency);
    hw.reset(other.hw->clone());
    connect(rdsTimer, &QTimer::timeout, this, &Radio::checkRDS);
    connect(this, &Radio::frequencyChanged, hw.get(), &IRadioHW::setFrequency);
    rdsTimer->stop();
    rdsTimer->start(other.rdsTimer->interval());
    return *this;
} 

Radio& Radio::operator=(Radio&& other) {
    minFrequency = other.minFrequency;
    maxFrequency = other.maxFrequency;
    setFrequency(other.frequency);
    hw = std::move(other.hw);
    delete rdsTimer;
    rdsTimer = other.rdsTimer;
    other.rdsTimer = nullptr;
    connect(rdsTimer, &QTimer::timeout, this, &Radio::checkRDS);
    connect(this, &Radio::frequencyChanged, hw.get(), &IRadioHW::setFrequency);
    return *this;
} 

Radio::~Radio() {
    delete rdsTimer;
    rdsTimer = nullptr;
}

void Radio::setFrequency(double frequency) {
    if (frequency < maxFrequency && frequency >= minFrequency) {
        qInfo() << "Radio: New frequency: " << frequency;
        double oldFreq = this->frequency;
        this->frequency = frequency;
        if (std::abs(frequency - oldFreq) > 0.01) {
            emit frequencyChanged(frequency);
        }
    }
}

void Radio::stepFrequency(double delta) {
    double newFrequency = frequency + delta;
    if (newFrequency >= maxFrequency)
        newFrequency = minFrequency + (newFrequency - maxFrequency);
    else if (newFrequency < minFrequency)
        newFrequency = maxFrequency - (minFrequency - newFrequency);
    qDebug() << "Radio: Stepping frequency to " << newFrequency;
    setFrequency(newFrequency);
}

void Radio::checkRDS() {
    QString old = rds;
    rds = hw->getRDS();
    if (old != rds) {
        qInfo() << "Radio: RDS changed:" << rds;
        emit RDSChanged(rds);
    }
}

QDebug operator<<(QDebug dbg, const Radio& r) {
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "(Radio: " << r.frequency;
    dbg << " MHz, Min: " << r.minFrequency << ", max: ";
    dbg << r.maxFrequency << ")";
    return dbg;
}
}
