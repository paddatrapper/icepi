/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of a virtual radio generating fake
 *                  information.
 *
 *        Created:  27/12/2018 16:13:46
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "virtual_radio.h"
#include <QDebug>

using namespace IcePi;

VirtualRadio::VirtualRadio() :
    rds("Test"),
    counter(0)
{}

VirtualRadio::VirtualRadio(const VirtualRadio& other) :
    rds(other.rds),
    counter(other.counter)
{}

VirtualRadio::VirtualRadio(VirtualRadio&& other) :
    rds(other.rds),
    counter(other.counter)
{}

VirtualRadio& VirtualRadio::operator=(const VirtualRadio& other) {
    rds = other.rds;
    counter = other.counter;
    return *this;
}

VirtualRadio& VirtualRadio::operator=(VirtualRadio&& other) {
    rds = other.rds;
    counter = other.counter;
    return *this;
}

QString VirtualRadio::getRDS() {
    ++counter;
    if (counter > 10) {
        qDebug() << "VirtualRadio: Toggle RDS";
        rds = (rds == "Test") ? "1 2 3" : "Test";
        counter = 0;
    }
    return rds;
}

IRadioHW* VirtualRadio::clone() {
    return new VirtualRadio(*this);
}

void VirtualRadio::setFrequency(double frequency) {
    qDebug() << "VirtualRadio: New hardware frequency is" << frequency;
}
