/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A hardware radio chip
 *
 *        Created:  27/12/2018 16:07:05
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef RADIO_HW_H
#define RADIO_HW_H

#include <QObject>
#include <QtPlugin>
#include <QString>

namespace IcePi {
    class IRadioHW : public QObject {
        public:
            virtual QString getRDS() = 0;
            virtual IRadioHW* clone() = 0;

        public slots:
            virtual void setFrequency(double frequency) = 0;
    };
}
Q_DECLARE_INTERFACE(IcePi::IRadioHW, "com.icepi.IRadioHW");
#endif

