/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A virtual radio generating fake information
 *
 *        Created:  27/12/2018 16:11:57
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef VIRTUAL_RADIO_H
#define VIRTUAL_RADIO_H

#include <QObject>
#include "radio_hw.h"

namespace IcePi {
    class VirtualRadio : public IRadioHW {
        Q_OBJECT;
        Q_INTERFACES(IcePi::IRadioHW);
        public:
            VirtualRadio();
            VirtualRadio(const VirtualRadio& other);
            VirtualRadio(VirtualRadio&& other);
            VirtualRadio& operator=(const VirtualRadio& other);
            VirtualRadio& operator=(VirtualRadio&& other);
            ~VirtualRadio() {}
            QString getRDS();
            IRadioHW* clone();

        public slots:
            void setFrequency(double frequency);

        private:
            QString rds;
            int counter;
    };
}
#endif
