/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Software-based radio
 *
 *        Created:  26/12/2018 17:46:16
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef RADIO_H
#define RADIO_H

#include <QObject>
#include <QString>
#include <QTimer>
#include <memory>

#include "hw/radio_hw.h"
#include "hw/virtual_radio.h"

namespace IcePi {
    class Radio : public QObject {
        Q_OBJECT;
        public:
            Radio();
            Radio(double minFrequency, double maxFrequency);
            Radio(const Radio& radio);
            Radio(Radio&& radio);
            double getFrequency() const { return frequency; };
            QString getRDS() const { return rds; };

            Radio& operator=(const Radio& other);
            Radio& operator=(Radio&& other);

            friend QDebug operator<<(QDebug dbg, const Radio& r);

            ~Radio();
        public slots:
            void setFrequency(double frequency);
            void stepFrequency(double delta);

        signals:
            void frequencyChanged(double newFrequency);
            void RDSChanged(QString newRDS);

        private slots:
            void checkRDS();

        private:
            double frequency;
            double minFrequency;
            double maxFrequency;
            QTimer* rdsTimer;
            QString rds;
            std::unique_ptr<IRadioHW> hw = std::make_unique<VirtualRadio>();
    };
}
#endif
