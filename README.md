# IcePi
IcePi is an in-car entertainment system built around the RaspberryPi 3 B+
running ARM64. This can be tested by installing
[Debian Buster](https://people.debian.org/~gwolf/raspberrypi3/)
on the Pi. Images including IcePi be available once there is a release.

# Building
This guide has been tested on Debian Sid:

```
$ sudo dpkg --add-architecture arm64
$ sudo apt update
$ sudo apt install cmake qt5-default qtbase5-dev:arm64 qtcreator \
  g++-aarch64-linux-gnu
$ mkdir build
$ cd build
$ cmake ..
$ make
```

In order to set up the Pi, you need to run

```
$ ssh root@pi "bash -s" < ./scripts/setup-pi
$ ssh root@pi "passwd user"
```

And finally copy over the binary and restart the pi

```
$ scp build/interface/icepi-interface user@pi:~/bin/
$ ssh root@pi "systemctl reboot"
```
