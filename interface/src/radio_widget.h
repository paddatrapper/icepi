/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Widget displaying a radio interface
 *
 *        Created:  26/12/2018 18:16:37
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef RADIO_WIDGET_H
#define RADIO_WIDGET_H
#include <QItemSelection>
#include <QString>
#include <QWidget>

#include "ui_radio_widget.h"

namespace Ui {
    class RadioWidget;
}

namespace IcePiInterface {
    class RadioWidget : public QWidget {
        Q_OBJECT;
        public:
            RadioWidget(QWidget* parent = nullptr);
            ~RadioWidget();

        public slots:
            void displayFrequency(double frequency);
            void displayRDS(QString rds);

        signals:
            void frequencySelected(double frequency);
            void frequencyStepped(double delta);

        private slots:
            void downClicked(bool checked);
            void upClicked(bool checked);
            void saveStation();
            void savedStationSelectionChanged(const QItemSelection& selected,
                const QItemSelection& deselected);

        private:
            Ui::RadioWidget* ui;
    };
}
#endif
