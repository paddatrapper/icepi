/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of a media player widget
 *
 *        Created:  30/12/2018 14:26:55
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "media_widget.h"
#include "ui_media_widget.h"

#include <QFileSystemModel>

using namespace IcePiInterface;

MediaWidget::MediaWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::MediaWidget)
{
    ui->setupUi(this);
    QFileSystemModel* model = new QFileSystemModel;
    model->setRootPath("/home/user/music");
    ui->treMedia->setModel(model);
    ui->treMedia->setRootIndex(model->index("/home/user/music"));
    for (int i = 1; i < model->columnCount(); ++i)
        ui->treMedia->hideColumn(i);
}

MediaWidget::~MediaWidget() {
    delete ui;
    ui = nullptr;
}
