/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Widget displaying a local media player
 *
 *        Created:  30/12/2018 14:21:05
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef MEDIA_WIDGET_H
#define MEDIA_WIDGET_H
#include <QWidget>

#include "ui_media_widget.h"

namespace Ui {
    class MediaWidget;
}

namespace IcePiInterface {
    class MediaWidget : public QWidget {
        Q_OBJECT;
        public:
            MediaWidget(QWidget* parent = nullptr);
            ~MediaWidget();

        private:
            Ui::MediaWidget* ui;
    };
}
#endif
