/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A clock displaying the time
 *
 *        Created:  26/12/2018 17:01:19
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef DIGITAL_CLOCK_H
#define DIGITAL_CLOCK_H

#include <QLCDNumber>
#include <QTimer>

namespace IcePiInterface {
    class DigitalClock : public QLCDNumber {
        Q_OBJECT;

        public:
            DigitalClock(QWidget *parent = nullptr);
            DigitalClock(const DigitalClock& clock);
            DigitalClock(DigitalClock&& clock);

            DigitalClock& operator=(const DigitalClock& other);
            DigitalClock& operator=(DigitalClock&& other);
            ~DigitalClock();
        private slots:
            void showTime();

        private:
            QTimer* timer;
    };
}
#endif
