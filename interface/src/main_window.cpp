/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of the main window
 *
 *        Created:  23/12/2018 18:35:25
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include <QTimer>
#include <QDebug>
#include <QSettings>

#include "media_widget.h"
#include "radio_widget.h"
#include "main_window.h"

using namespace IcePiInterface;

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    radio(IcePi::Radio(87.5, 108.1))
{
    QSettings settings;
    double frequency = settings.value("radio/currentFrequency").toDouble();
    radio.setFrequency(frequency);
    ui->setupUi(this);
    qDebug() << "MainWindow: Setup Main Window";
    // Enable full screen display
    QTimer::singleShot(0, this, SLOT(showFullScreen()));
    connect(ui->btnRadio, &QPushButton::clicked,
            this, &MainWindow::radioStarted);
    connect(ui->btnMedia, &QPushButton::clicked,
            this, &MainWindow::mediaStarted);
    mediaStarted(true);
}

MainWindow::~MainWindow() {
    delete ui;
    ui = nullptr;
}

void MainWindow::radioStarted(bool checked) {
    ui->btnRadio->setChecked(true);
    ui->btnMedia->setChecked(false);
    ui->btnVideo->setChecked(false);
    ui->btnPhone->setChecked(false);
    ui->btnNavigation->setChecked(false);
    ui->btnSettings->setChecked(false);
    if (!checked) return; // Do nothing further if already selected mode
    qInfo() << "MainWindow: Selecting radio widget";
    QWidget* oldPanel = ui->pnlRight;
    RadioWidget* r = new RadioWidget(oldPanel->parentWidget());
    QLayoutItem* old = ui->grdMain->replaceWidget(oldPanel, r);
    if (old) delete old;
    ui->pnlRight = r;
    old = nullptr;
    delete oldPanel;
    oldPanel = nullptr;
    connect(r, &RadioWidget::frequencyStepped,
            &radio, &IcePi::Radio::stepFrequency);
    connect(&radio, &IcePi::Radio::frequencyChanged,
            r, &RadioWidget::displayFrequency);
    connect(r, &RadioWidget::frequencySelected,
            &radio, &IcePi::Radio::frequencyChanged);
    connect(&radio, &IcePi::Radio::RDSChanged,
            r, &RadioWidget::displayRDS);
    r->displayFrequency(radio.getFrequency());
}

void MainWindow::mediaStarted(bool checked) {
    ui->btnRadio->setChecked(false);
    ui->btnMedia->setChecked(true);
    ui->btnVideo->setChecked(false);
    ui->btnPhone->setChecked(false);
    ui->btnNavigation->setChecked(false);
    ui->btnSettings->setChecked(false);
    if (!checked) return; // Do nothing further if already selected mode
    qInfo() << "MainWindow: Selecting media widget";
    QWidget* oldPanel = ui->pnlRight;
    MediaWidget* m = new MediaWidget(oldPanel->parentWidget());
    QLayoutItem* old = ui->grdMain->replaceWidget(oldPanel, m);
    if (old) delete old;
    ui->pnlRight = m;
    old = nullptr;
    delete oldPanel;
    oldPanel = nullptr;
}
