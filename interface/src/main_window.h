/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  The main window displayed when the application starts
 *
 *        Created:  23/12/2018 18:31:59
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>
#include <QWidget>

#include "radio.h"

#include "ui_main_window.h"

namespace Ui {
    class MainWindow;
}

namespace IcePiInterface {
    class MainWindow : public QMainWindow {
        Q_OBJECT;
        public:
            MainWindow(QWidget* parent = nullptr);
            ~MainWindow();

        private slots:
            void radioStarted(bool checked);
            void mediaStarted(bool checked);

        private:
            Ui::MainWindow* ui;
            IcePi::Radio radio;
    };
}
#endif
