/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implentation of a radio widget
 *
 *        Created:  26/12/2018 18:21:49
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include <QSettings>
#include <QString>
#include <QAbstractListModel>
#include <QDebug>

#include "radio_widget.h"
#include "model/radio_station_list.h"
#include "ui_radio_widget.h"
#include "model/radio_station.h"

using namespace IcePiInterface;

RadioWidget::RadioWidget(QWidget* parent) :
    QWidget(parent),
    ui(new Ui::RadioWidget)
{
    ui->setupUi(this);
    QItemSelectionModel* m = ui->lstStations->selectionModel();
    ui->lstStations->setModel(new RadioStationList());
    delete m;
    QItemSelectionModel* selection = ui->lstStations->selectionModel();
    m = nullptr;
    connect(ui->btnDown, &QPushButton::clicked,
            this, &RadioWidget::downClicked);
    connect(ui->btnUp, &QPushButton::clicked,
            this, &RadioWidget::upClicked);
    connect(ui->btnSave, &QPushButton::clicked,
            this, &RadioWidget::saveStation);
    connect(selection, &QItemSelectionModel::selectionChanged,
            this, &RadioWidget::savedStationSelectionChanged);
}

void RadioWidget::displayFrequency(double frequency) {
    QString text;
    text.sprintf("%.2f", frequency);

    ui->lcdFrequency->display(text);
    RadioStation test { "", frequency };
    QVariant value = QVariant::fromValue(test);

    QAbstractItemModel* model = ui->lstStations->model();
    QModelIndex start = model->index(0, 0);
    QItemSelectionModel* selection = ui->lstStations->selectionModel();
    QModelIndexList matches = model->match(start, Qt::DisplayRole, value);
    if (matches.size() > 0) {
        selection->select(matches.at(0), QItemSelectionModel::ClearAndSelect);
        QIcon icon { ":/resources/md-icons/remove.svg" };
        ui->btnSave->setIcon(icon);
    } else {
        selection->clear();
        QIcon icon { ":/resources/md-icons/add.svg" };
        ui->btnSave->setIcon(icon);
    } 
    QSettings settings;
    settings.setValue("radio/currentFrequency", frequency);
}

void RadioWidget::displayRDS(QString rds) {
    qInfo() << "RadioWidget: New RDS information:" << rds;
    ui->lblStation->setText(rds);
}

void RadioWidget::downClicked(bool checked) {
    qDebug() << "RadioWidget: Radio frequency down clicked";
    emit frequencyStepped(-0.1);
}

void RadioWidget::upClicked(bool checked) {
    qDebug() << "RadioWidget: Radio frequency up clicked";
    emit frequencyStepped(0.1);
}

void RadioWidget::saveStation() {
    qDebug() << "RadioWidget: Save station clicked";
    QString name { ui->lblStation->text() };
    double frequency { ui->lcdFrequency->value() };
    RadioStation station { name, frequency };
    QVariant newValue = QVariant::fromValue(station);
    RadioStationList* model =
        static_cast<RadioStationList*>(ui->lstStations->model());
    QModelIndex start = model->index(0, 0);
    QModelIndexList matches = model->match(start, Qt::DisplayRole, newValue); 
    QSettings settings;
    settings.beginGroup("radio");
    if (matches.size() == 0) {
        // Save station
        model->insertRow(model->rowCount());
        QModelIndex i = model->index(model->rowCount() - 1, 0);
        qInfo() << "RadioWidget: Saving" << station << "to" << i;
        model->setData(i, QVariant::fromValue(station));
        QItemSelectionModel* selection = ui->lstStations->selectionModel();
        selection->select(i, QItemSelectionModel::ClearAndSelect);
    } else {
        // Delete station
        QModelIndex i = matches.at(0);
        model->removeRow(i.row());
        if (model->isEmpty()) {
            QIcon icon { ":/resources/md-icons/add.svg" };
            ui->btnSave->setIcon(icon);
        }
    }
}

void RadioWidget::savedStationSelectionChanged(const QItemSelection& selected,
        const QItemSelection& deselected) {
    if (selected.indexes().size() == 1) {
        QVariant value =
            ui->lstStations->model()->data(selected.indexes().at(0),
                    Qt::UserRole);
        RadioStation station = qvariant_cast<RadioStation>(value);
        qInfo() << "RadioWidget: Selecting station" << station;
        emit frequencySelected(station.getFrequency());
    }
}

RadioWidget::~RadioWidget() {
    delete ui;
    ui = nullptr;
}
