/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  The main interface to interact with IcePi
 *
 *        Created:  23/12/2018 16:32:40
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include <QApplication>
#include <QCoreApplication>

#include "main_window.h"

using namespace IcePiInterface;

int main(int argc, char* argv[]) {
    QCoreApplication::setOrganizationName("BitCast");
    QCoreApplication::setOrganizationDomain("bitcast.co.za");
    QCoreApplication::setApplicationName("IcePi");
    QApplication app { argc, argv };
    MainWindow window;
    window.setWindowTitle("IcePi");
    window.show();

    return app.exec();
}
