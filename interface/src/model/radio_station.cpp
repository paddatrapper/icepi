/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implentation of a radio station
 *
 *        Created:  27/12/2018 20:32:05
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "radio_station.h"

#include <cmath>

namespace IcePiInterface {

    RadioStation::RadioStation(QString name, double frequency) :
        name(name),
        frequency(frequency)
    {}

    RadioStation::RadioStation() : RadioStation("", 0.0) {}

    RadioStation::RadioStation(const RadioStation& other) :
        name(other.name),
        frequency(other.frequency)
    {}

    RadioStation::RadioStation(RadioStation&& other) :
        name(other.name),
        frequency(other.frequency)
    {}

    QString RadioStation::toString() const {
        return name + QString::asprintf(": %.2fFM", frequency);
    }

    RadioStation& RadioStation::operator=(const RadioStation& other) {
        name = other.name;
        frequency = other.frequency;
        return *this;
    }

    RadioStation& RadioStation::operator=(RadioStation&& other) {
        name = other.name;
        frequency = other.frequency;
        return *this;
    }

    bool RadioStation::operator==(const RadioStation& other) const {
        return std::abs(frequency - other.frequency) < 0.001;
    }

    bool RadioStation::operator!=(const RadioStation& other) const {
        return !(*this == other);
    }

    QDebug operator<<(QDebug dbg, const RadioStation& station) {
        QDebugStateSaver saver(dbg);
        dbg << station.toString();
        return dbg;
    }
}
