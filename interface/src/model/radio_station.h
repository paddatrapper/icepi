/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A radio station
 *
 *        Created:  27/12/2018 19:20:40
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef RADIO_STATION_H
#define RADIO_STATION_H

#include <QDebug>
#include <QObject>
#include <QString>

namespace IcePiInterface {
    class RadioStation : public QObject {
        Q_OBJECT;
        public:
            RadioStation();
            RadioStation(QString name, double frequency);
            RadioStation(const RadioStation& other);
            RadioStation(RadioStation&& other);
            QString getName() const { return name; }
            double getFrequency() const { return frequency; }
            void setName(QString name) { this->name = name; }
            void setFrequency(double frequency) { this->frequency = frequency; }
            QString toString() const;

            RadioStation& operator=(const RadioStation& other);
            RadioStation& operator=(RadioStation&& other);
            bool operator==(const RadioStation& other) const;
            bool operator!=(const RadioStation& other) const;
            friend QDebug operator<<(QDebug dbg, const RadioStation& station);
        private:
            QString name;
            double frequency;
    };
}
Q_DECLARE_METATYPE(IcePiInterface::RadioStation)
#endif
