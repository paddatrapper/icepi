/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of a list of radio stations
 *
 *        Created:  27/12/2018 19:39:44
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "radio_station_list.h"
#include <QDebug>
#include <QSettings>
#include <thread>

using namespace IcePiInterface;

void saveStations(QVector<RadioStation> stations) {
    QSettings settings;
    settings.beginWriteArray("radio/favourites");
    for (int i = 0; i < stations.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue("name", stations.at(i).getName());
        settings.setValue("frequency", stations.at(i).getFrequency());
    }
    settings.endArray();
}

RadioStationList::RadioStationList(QObject* parent) :
    QAbstractListModel(parent)
{
    stations.clear();
    QSettings settings;
    int size = settings.beginReadArray("radio/favourites");
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        QString name = settings.value("name").toString();
        double frequency = settings.value("frequency").toDouble();
        RadioStation station { name, frequency };
        stations.append(station);
    }
    settings.endArray();
}

int RadioStationList::rowCount(const QModelIndex& parent) const {
    return stations.size();
}

QVariant RadioStationList::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) return QVariant();
    if (index.row() >= stations.size()) return QVariant();
    if (role == Qt::DisplayRole)
        return stations.at(index.row()).toString();
    if (role == Qt::UserRole)
        return QVariant::fromValue(stations.at(index.row()));
    return QVariant();
}

QVariant RadioStationList::headerData(int section, Qt::Orientation orientation,
        int role) const {
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
        return QString("Favourite Radio Stations");
    return QVariant();
}

bool RadioStationList::setData(const QModelIndex& index, const QVariant& value,
        int role) {
    if (!index.isValid()) return false;
    if (role == Qt::EditRole) {
        qInfo() << "RadioStationList: Setting" << index << "to" << value;
        RadioStation old = stations.at(index.row());
        stations.replace(index.row(), qvariant_cast<RadioStation>(value));
        // TODO: Fix assert failure...
        std::thread t { saveStations, stations };
        emit dataChanged(index, index, {role});
        t.join();
        return true;
    }
    return false;
}

bool RadioStationList::insertRows(int row, int count,
        const QModelIndex& parent) {
    beginInsertRows(QModelIndex(), row, row+count-1);
    stations.insert(row, count, RadioStation());
    endInsertRows();
    return true;
}

bool RadioStationList::removeRows(int row, int count,
        const QModelIndex& parent) {
    beginRemoveRows(QModelIndex(), row, row+count-1);
    for (int i = 0; i < count; ++i) {
        stations.remove(row); 
    }
    endRemoveRows();
    std::thread t { saveStations, stations };
    t.join();
    return true;
}

QModelIndexList RadioStationList::match(const QModelIndex& start,
        int role, const QVariant& value, int hits,
        Qt::MatchFlags flags) const {
    QModelIndexList list;
    const RadioStation target = qvariant_cast<RadioStation>(value);
    for (auto i = start.row(); i < stations.size(); ++i) {
        if (stations.at(i) == target) {
            list.append(createIndex(i, 0));
        }
    }
    return list;
}

bool RadioStationList::isEmpty() const {
    return stations.isEmpty();
}
