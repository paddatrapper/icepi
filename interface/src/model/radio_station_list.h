/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A list of radio stations
 *
 *        Created:  27/12/2018 19:28:45
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#ifndef RADIO_STATION_LIST_H
#define RADIO_STATION_LIST_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include "radio_station.h"

namespace IcePiInterface {
    class RadioStationList : public QAbstractListModel {
        Q_OBJECT;
        public:
            RadioStationList(QObject* parent = nullptr);
            int rowCount(const QModelIndex& parent = QModelIndex()) const override;
            QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
            QVariant headerData(int section, Qt::Orientation orientation,
                    int role = Qt::DisplayRole) const override;
            bool setData(const QModelIndex& index, const QVariant& value,
                    int role = Qt::EditRole) override;
            bool insertRows(int row, int count,
                    const QModelIndex& parent = QModelIndex()) override;
            bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
            QModelIndexList match(const QModelIndex& start, int role, const QVariant& value,
                    int hits = 1,
                    Qt::MatchFlags flags = Qt::MatchFlags(Qt::MatchStartsWith|Qt::MatchWrap))
                const override;
            bool isEmpty() const;

        private:
            QVector<RadioStation> stations;
    };
}
#endif
