/*******************************************************************************
 * Copyright (c) 2018, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementation of a digital wall clock
 *
 *        Created:  26/12/2018 17:03:38
 *       Compiler:  aarch64-linux-gnu-g++
 *
 *         Author:  Kyle Robbertze (kr), kyle@paddatrapper.com
 *******************************************************************************/
#include "digital_clock.h"

#include <QTime>

using namespace IcePiInterface;

DigitalClock::DigitalClock(QWidget *parent) : QLCDNumber(parent) {
    // Start clock
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    timer->start(1000);
    showTime();
}

DigitalClock::DigitalClock(const DigitalClock& clock) :
    QLCDNumber(clock.parentWidget())
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    timer->start(clock.timer->interval());
    showTime();
}

DigitalClock::DigitalClock(DigitalClock&& clock) :
    QLCDNumber(clock.parentWidget())
{
    timer = clock.timer;
    clock.timer = nullptr;
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    showTime();
}

DigitalClock& DigitalClock::operator=(const DigitalClock& clock)
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    timer->start(clock.timer->interval());
    showTime();
    return *this;
}

DigitalClock& DigitalClock::operator=(DigitalClock&& clock)
{
    timer = clock.timer;
    clock.timer = nullptr;
    connect(timer, &QTimer::timeout, this, &DigitalClock::showTime);
    showTime();
    return *this;
}

DigitalClock::~DigitalClock() {
    delete timer;
    timer = nullptr;
}

void DigitalClock::showTime() {
    QTime time = QTime::currentTime();
    QString text = time.toString("HH:mm");
    if ((time.second() % 2) == 0)
        text[2] = ' ';
    display(text);

}
